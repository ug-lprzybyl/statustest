---
        title: Niedostępność jira.unity.pl
        date: 2023-05-18 19:00:00
        resolved: true
        # Possible severity levels: down, disrupted, notice
        severity: notice
        affected:
        - JIRA
        section: issue
        resolvedWhen: 2023-05-18
---

Last updated: 26/07/2023 07:06:08