---
        title: Aktualizacja firmware fortigate fg01
        date: 2023-06-12 21:30:00
        resolved: true
        # Possible severity levels: down, disrupted, notice
        severity: notice
        affected:
        - VPN
        section: issue
        resolvedWhen: 2023-06-13T13:37:35.000+0200
---
W związku z podatnością CVE-2023-27997 zostanie podniesiony firmware fg01 do wersji 7.2.5.

Last updated: 26/07/2023 07:06:08