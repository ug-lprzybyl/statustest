---
        title: Restart confluence.unity.pl
        date: 2023-05-18 20:00:00
        resolved: true
        # Possible severity levels: down, disrupted, notice
        severity: notice
        affected:
        - CONFLUENCE
        section: issue
        resolvedWhen: 2023-05-18
---
ITW-25649

Last updated: 26/07/2023 07:06:08