---
        title: Niedostępność jira.unity.pl
        date: 2023-06-01 10:39:00
        resolved: false
        # Possible severity levels: down, disrupted, notice
        severity: notice
        affected:
        - JIRA
        section: issue
        resolvedWhen: 2023-06-01
---
@here Planowane prace na Jira - Dzisiaj ok. godziny 19.00 nastąpi restart Jiry - ze względu na zmiany w dodatkach do Jiry - niedostępność ok 15 min

Last updated: 26/07/2023 07:06:08