---
        title: Server down!
        date: 2023-07-21 21:34:00
        resolved: false
        # Possible severity levels: down, disrupted, notice
        severity: notice
        affected:
        - BIURO_WRO
        section: issue
        resolvedWhen: 2023-07-23
---
This is a description 

Last updated: 26/07/2023 07:06:08